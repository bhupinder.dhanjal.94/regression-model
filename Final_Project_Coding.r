#################################################

## Final Project - Regression Modelling (6557) ##

###### Bhupinder Singh Dhanjal - u3211361 #######

############### Coding Section ##################




# Loading Data and Packages
library(tidyverse)
library(caret)
library(modelr)
library(GGally)

redwine <- read_csv('winequality-red.csv')
whitewine <- read_csv('winequality-white.csv')
colnames(redwine)
colnames(whitewine)

# Correlation Heat Maps

# a. RedWine
ggcorr(redwine, label = T)

# b. Whitewine
ggcorr(whitewine, label = T)



# Partitioning Data in Train and Test Datasets

# a. Redwine
set.seed(123)
rsample <- redwine$quality %>%
  createDataPartition(p = 0.7, list = F)
rtrain <- redwine[rsample,]
rtest <- redwine[-rsample,]

# b. Whitewine
set.seed(123)
wsample <- whitewine$quality %>%
  createDataPartition(p = 0.7, list = F)
wtrain <- whitewine[rsample,]
wtest <- whitewine[-rsample,]



########    MODELLING PART


### 1. Simple Linear Regression Model

# a. Redwine
rlm <- lm(quality~alcohol, data = rtrain)
summary(rlm)
rlmpredictions <- rlm %>% predict(rtest)
rlmsummary <- data.frame(Model = 'Simple Linear Regression',
                         RMSE = caret::RMSE(rlmpredictions, rtest$quality),
                         RSQ = caret::R2(rlmpredictions, rtest$quality))
rlmsummary

# b. Whitewine
wlm <- lm(quality~alcohol, data = wtrain)
summary(wlm)
wlmpredictions <- wlm %>% predict(wtest)
wlmsummary <- data.frame(Model = 'Simple Linear Regression',
                         RMSE = caret::RMSE(wlmpredictions, wtest$quality),
                         RSQ = caret::R2(wlmpredictions, wtest$quality))
wlmsummary


### 2. Multiple Linear Regression

# a. Redwine
rmlm <- lm(quality~., data = rtrain)
summary(rmlm)
rmlmpredictions <- rmlm %>% predict(rtest)
rmlmsummary <- data.frame(Model = 'Multiple Linear Regression',
                          RMSE = caret::RMSE(rmlmpredictions, rtest$quality),
                          RSQ = caret::R2(rmlmpredictions, rtest$quality))
rmlmsummary

# b. Whitewine
wmlm <- lm(quality~., data = wtrain)
summary(wmlm)
wmlmpredictions <- wmlm %>% predict(wtest)
wmlmsummary <- data.frame(Model = 'Multiple Linear Regression',
                          RMSE = caret::RMSE(wmlmpredictions, wtest$quality),
                          RSQ = caret::R2(wmlmpredictions, wtest$quality))
wmlmsummary


### 3. Logarithmic Transformation
hist(redwine$alcohol, main = 'Histogram of Alcohol in Red Wine')
hist(whitewine$alcohol, main = 'Histogram of Alcohol in White Wine')
rlogalcohol <- log(rtrain$alcohol)
rtest$rlogalcohol <- log(rtest$alcohol)
wlogalcohol <- log(wtrain$alcohol)
wtest$wlogalcohol <- log(wtest$alcohol)
hist(rlogalcohol, main = 'Histogram of Alcohol in Red Wine, with Log Transf.')
hist(wlogalcohol, main = 'Histogram of Alcohol in White Wine, with Log Transf.')
#Both look better.

# a. Redwine
rlogm <- lm(quality~rlogalcohol, data = rtrain)
summary(rlogm)
rlogmpredictions <- rlogm %>% predict(rtest)
rlogmsummary <- data.frame(Model = 'Logarithmic Tranf.',
                           RMSE = caret::RMSE(rlogmpredictions, rtest$quality),
                           RSQ =caret:: R2(rlogmpredictions, rtest$quality))
rlogmsummary

# b. Whitewine
wlogm <- lm(quality~wlogalcohol, data = wtrain)
summary(wlogm)
wlogmpredictions <- wlogm %>% predict(wtest)
wlogmsummary <- data.frame(Model = 'Logarithmic Tranf.',
                           RMSE = caret::RMSE(wlogmpredictions, wtest$quality),
                           RSQ = caret::R2(wlogmpredictions, wtest$quality))
wlogmsummary


### 4. PC Regression

# a. Redwine
library(pls)
set.seed(123)
rpcrm <- train(quality~.,rtrain,
               method = 'pcr', scale=TRUE,
               trControl = trainControl("cv",number = 5),
               tuneLength=10)
plot(rpcrm)
rpcrm$bestTune
summary(rpcrm$finalModel)
rpcrmpredictions <- rpcrm %>% predict(rtest)
rpcrmsummary <- data.frame(Model = 'PC Regression',
                           RMSE = caret::RMSE(rpcrmpredictions, rtest$quality),
                           RSQ = caret::R2(rpcrmpredictions, rtest$quality))
rpcrmsummary

# b. Whitewine
library(pls)
set.seed(123)
wpcrm <- train(quality~.,wtrain,
               method = 'pcr', scale=TRUE,
               trControl = trainControl("cv",number = 5),
               tuneLength=10)
plot(wpcrm)
wpcrm$bestTune
summary(wpcrm$finalModel)
wpcrmpredictions <- wpcrm %>% predict(wtest)
wpcrmsummary <- data.frame(Model = 'PC Regression',
                           RMSE = caret::RMSE(wpcrmpredictions, wtest$quality),
                           RSQ = caret::R2(wpcrmpredictions, wtest$quality))
wpcrmsummary


### 5. K-NN Regression

# a. Redwine
rknnrm <- train(quality~., rtrain, method = 'knn',
                trControl = trainControl('cv',number = 5),
                tuneLength = 20)
plot(rknnrm)
rknnrm$bestTune

rknnrmprediction <- predict(rknnrm, rtest)
rknnrmsummary <- data.frame(Model = 'K-NN Regression',
                            RMSE = caret::RMSE(rknnrmprediction,rtest$quality),
                            RSQ = caret::R2(rknnrmprediction,rtest$quality))
rknnrmsummary

# b. Whitewine
wknnrm <- train(quality~., wtrain, method = 'knn',
                trControl = trainControl('cv',number = 5),
                tuneLength = 20)
plot(wknnrm)
wknnrm$bestTune

wknnrmprediction <- predict(wknnrm, wtest)
wknnrmsummary <- data.frame(Model = 'K-NN Regression',
                            RMSE = caret::RMSE(wknnrmprediction,wtest$quality),
                            RSQ = caret::R2(wknnrmprediction,wtest$quality))
wknnrmsummary


### 6. K-NN Classifier

# a. Redwine
RTRAIN <- rtrain
RTEST <- rtest 
RTRAIN$quality <- as.factor(RTRAIN$quality)
RTEST$quality <- as.factor(RTEST$quality)

rknncm <- train(quality~., RTRAIN, method = 'knn',
                trControl = trainControl('cv',number = 5),
                tuneLength = 20)
plot(rknncm)
rknncm$bestTune

rknncmprediction <- predict(rknncm, RTEST)
rknnacc <- mean(rknncmprediction == RTEST$quality)
rknnacc

# b. Whitewine
WTRAIN <- wtrain
WTEST <- wtest
WTRAIN$quality <- as.factor(WTRAIN$quality)
WTEST$quality <- as.factor(WTEST$quality)

wknncm <- train(quality~., WTRAIN, method = 'knn',
                trControl = trainControl('cv',number = 5),
                tuneLength = 20)
plot(wknncm)
wknncm$bestTune

wknncmprediction <- predict(wknncm, WTEST)
wknnacc <- mean(wknncmprediction == WTEST$quality)
wknnacc


### 7. Decision Tree Classifier

# a. Redwine
library(rpart)
library(rpart.plot)
rcartmodel <- rpart(quality~., RTRAIN,
                    method = 'class')
rpart.plot(rcartmodel, type = 4, extra = 2)
rcm <- predict(rcartmodel, rtest, type = 'class')

rdtacc <- mean(rcm == RTEST$quality)
rdtacc

# b. Whitewine
wcartmodel <- rpart(quality~., WTRAIN,
                    method = 'class')
rpart.plot(wcartmodel, type = 4, extra = 2)
wcm <- predict(wcartmodel, WTEST, type = 'class')

wdtacc <- mean(wcm == WTEST$quality)
wdtacc



######   Classification Models after Releveling of Quality Variable
### Releveling Red Wine Dataset
rtr <- rtrain

rtr$quality[rtr$quality == 3] = 'Low'
rtr$quality[rtr$quality == 4] = 'Low'
rtr$quality[rtr$quality == 5] = 'Medium'
rtr$quality[rtr$quality == 6] = 'Medium'
rtr$quality[rtr$quality == 7] = 'High'
rtr$quality[rtr$quality == 8] = 'High'

rtr$quality <- as.factor(rtr$quality)

rts <- rtest

rts$quality[rts$quality == 3] = 'Low'
rts$quality[rts$quality == 4] = 'Low'
rts$quality[rts$quality == 5] = 'Medium'
rts$quality[rts$quality == 6] = 'Medium'
rts$quality[rts$quality == 7] = 'High'
rts$quality[rts$quality == 8] = 'High'

rts$quality <- as.factor(rts$quality)

### Releveling White Wine Dataset
wtr <- wtrain

wtr$quality[wtr$quality == 3] = 'Low'
wtr$quality[wtr$quality == 4] = 'Low'
wtr$quality[wtr$quality == 5] = 'Medium'
wtr$quality[wtr$quality == 6] = 'Medium'
wtr$quality[wtr$quality == 7] = 'High'
wtr$quality[wtr$quality == 8] = 'High'

wtr$quality <- as.factor(wtr$quality)

wts <- wtest

wts$quality[wts$quality == 3] = 'Low'
wts$quality[wts$quality == 4] = 'Low'
wts$quality[wts$quality == 5] = 'Medium'
wts$quality[wts$quality == 6] = 'Medium'
wts$quality[wts$quality == 7] = 'High'
wts$quality[wts$quality == 8] = 'High'

wts$quality <- as.factor(wts$quality)


### K-NN Classifier on Releveled Datasets
# a. Redwine
rknncm <- train(quality~., rtr, method = 'knn',
                trControl = trainControl('cv',number = 5),
                tuneLength = 20)
plot(rknncm)
rknncm$bestTune

rknncmprediction <- predict(rknncm, rts)
rknnacc <- mean(rknncmprediction == rts$quality)
rknnacc

# b. Whitewine
wknncm <- train(quality~., wtr, method = 'knn',
                trControl = trainControl('cv',number = 5),
                tuneLength = 20)
plot(wknncm)
wknncm$bestTune

wknncmprediction <- predict(wknncm, wts)
wknnacc <- mean(wknncmprediction == wts$quality)
wknnacc


### Decision Tree Classifier on Releveled Dataset
# a. Redwine
rcartmodel <- rpart(quality~., rtr,
                    method = 'class')
rpart.plot(rcartmodel, type = 4, extra = 2)
rcm <- predict(rcartmodel, rts, type = 'class')

rdtacc <- mean(rcm == rts$quality)
rdtacc

# b. Whitewine
wcartmodel <- rpart(quality~., wtr,
                    method = 'class')
rpart.plot(wcartmodel, type = 4, extra = 2)
wcm <- predict(wcartmodel, wts, type = 'class')

wdtacc <- mean(wcm == wts$quality)
wdtacc



########    COMPARISON

# Comparing All the models 

# A. Redwine
rmodelrank <- data.frame(Model = c('Simple Linear Regression',
                                   'Multiple Linear Regression',
                                   'Logarithm Transf.',
                                   'PC Regression',
                                   'K-NN Regression',
                                   'K-NN Classifier',
                                   'Decision Tree Classifier'),
                         Accuracy_RSQ = c(rlmsummary[1,3],
                                          rmlmsummary[1,3],
                                          rlogmsummary[1,3],
                                          rpcrmsummary[1,3],
                                          rknnrmsummary[1,3],
                                          rknnacc,
                                          rdtacc)) %>%
  arrange(-`Accuracy_RSQ`)
rmodelrank

# b. Whitewine
wmodelrank <- data.frame(Model = c('Simple Linear Regression',
                                   'Multiple Linear Regression',
                                   'Logarithm Transf.',
                                   'PC Regression',
                                   'K-NN Regression',
                                   'K-NN Classifier',
                                   'Decision Tree Classifier'),
                         Accuracy_RSQ = c(wlmsummary[1,3],
                                          wmlmsummary[1,3],
                                          wlogmsummary[1,3],
                                          wpcrmsummary[1,3],
                                          wknnrmsummary[1,3],
                                          wknnacc,
                                          wdtacc)) %>%
  arrange(-`Accuracy_RSQ`)
wmodelrank
